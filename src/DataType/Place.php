<?php declare(strict_types=1);

namespace DataTypePlace\DataType;

use Laminas\Form\Element;
use Laminas\View\Renderer\PhpRenderer;
use Omeka\Api\Adapter\AbstractEntityAdapter;
use Omeka\Api\Representation\ValueRepresentation;
use Omeka\DataType\AbstractDataType;
use Omeka\DataType\DataTypeInterface;
use Omeka\DataType\ValueAnnotatingInterface;
use Omeka\Entity\Value;

class Place extends AbstractDataType implements DataTypeInterface, ValueAnnotatingInterface
{
    protected $keys = [
        'geonameId' => null,
        'toponym' => null,
        'country' => null,
        'latitude' => null,
        'longitude' => null,
        // "uri" may be used for isValid() and hydrate() when "@id" is missing.
    ];

    public function getName()
    {
        return 'place';
    }

    public function getLabel()
    {
        return 'Place'; // @translate
    }

    public function prepareForm(PhpRenderer $view): void
    {
        $assetUrl = $view->plugin('assetUrl');
        $view->headLink()->appendStylesheet($assetUrl('css/data-type-place.css', 'DataTypePlace'));
        $view->headScript()->appendFile($assetUrl('js/data-type-place.js', 'DataTypePlace'), 'text/javascript', ['defer' => 'defer']);
    }

    public function form(PhpRenderer $view)
    {
        $elementId = new Element\Hidden('@id');
        $elementId->setAttributes([
            'class' => 'value to-require',
            'data-value-key' => '@id',
        ]);
        $hiddenValue = new Element\Hidden('o:data');
        $hiddenValue->setAttributes([
            'class' => 'value to-require',
            'data-value-key' => 'o:data',
        ]);
        return '<div class="data-type-place"></div>'
            . $view->formHidden($elementId)
            . $view->formHidden($hiddenValue);
    }

    /**
     * A place is an extended uri ("@id" and "o:label") with data in "o:data".
     *
     * For compatibility with old versions and some modules like CSV Import,
     * check "@value" and "o:label" when "o:data" is missing.
     *
     * {@inheritDoc}
     * @see \Omeka\DataType\DataTypeInterface::isValid()
     */
    public function isValid(array $valueObject)
    {
        $val = $valueObject['o:data'] ?? $valueObject['@value'] ?? $valueObject['o:label'] ?? null;
        if (!$val) {
            return false;
        }

        $val = is_array($val) ? $val : json_decode($val, true);
        if (!$val
            || !is_array($val)
            || empty($val['geonameId'])
            || empty($val['toponym'])
            // Other keys are optional.
            // By default, the place is similar to value suggest geonames.
        ) {
            return false;
        }

        $valueObject['@id'] ??= $val['@id'] ?? $val['uri'] ?? null;
        if (empty($valueObject['@id'])
            || !is_string($valueObject['@id'])
            || trim($valueObject['@id']) === ''
            || !filter_var($valueObject['@id'], FILTER_VALIDATE_URL)
        ) {
            return false;
        }

        // Check extra keys for security.
        unset($val['@id'], $val['uri']);
        $v = array_diff_key($val, $this->keys);
        return !count($v);
    }

    public function hydrate(array $valueObject, Value $value, AbstractEntityAdapter $adapter): void
    {
        // The value object should be already checked.
        // For compatibility with old versions and some modules like CSV Import,
        // check "@value" and "o:label" when "o:data" is missing.
        $val = $valueObject['o:data'] ?? $valueObject['@value'] ?? $valueObject['o:label'] ?? null;
        $val = is_array($val) ? $val : json_decode($val, true);
        $valueObject['@id'] ??= $val['@id'] ?? $val['uri'] ?? '';
        $v = array_intersect_key($val, $this->keys);
        $val = json_encode($v, JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        $value->setUri(trim($valueObject['@id']));
        $value->setValue($val);
        // Set defaults.
        $value->setLang(null);
        $value->setValueResource(null);
    }

    public function render(PhpRenderer $view, ValueRepresentation $value)
    {
        $plugins = $view->getHelperPluginManager();
        $val = json_decode($value->value(), true);
        if (!$val || !is_array($val)) {
            $translate = $plugins->get('translate');
            return $translate('No data about the place'); // @translate
        }
        $escape = $plugins->get('escapeHtml');
        $hyperlink = $plugins->get('hyperlink');
        return $hyperlink($val['toponym'], $value->uri(), ['class' => 'uri-value-link', 'target' => '_blank'])
            . (empty($val['country']) ? '': ' (' . $escape($val['country']) . ')')
            . (array_key_exists('latitude', $val) && array_key_exists('longitude', $val) ? $escape(sprintf(' [%1$s/%2$s]', $val['latitude'], $val['longitude'])) : '');
    }

    /**
     * The data type "place" is a uri with data, not a literal.
     *
     * In Omeka, the value must be null, int or string because it is used in
     * many functions. But, the place is an array of data. So "o:data" is used
     * instead of "@value" and "o:label" is empty.
     *
     * {@inheritDoc}
     * @see \Omeka\DataType\DataTypeInterface::getJsonLd()
     */
    public function getJsonLd(ValueRepresentation $value)
    {
        $data = json_decode($value->value(), true);
        return [
            '@id' => $value->uri(),
            'o:label' => $data['toponym'],
            'o:data' => $data,
        ];
    }

    public function getFulltextText(PhpRenderer $view, ValueRepresentation $value)
    {
        return $value->uri() . ' ' . implode(' ', json_decode($value->value(), true));
    }

    public function valueAnnotationPrepareForm(PhpRenderer $view): void
    {
        $this->prepareForm($view);
    }

    public function valueAnnotationForm(PhpRenderer $view)
    {
        return $this->form($view);
    }

    public static function asLatLong(ValueRepresentation $value, string $separator = '/')
    {
        $val = json_decode($value->value(), true);
        if (!$val || !is_array($val) || !array_key_exists('latitude', $val) && !array_key_exists('longitude', $val)) {
            return '';
        }
        return $val['latitude'] . $separator . $val['longitude'];
    }
}
