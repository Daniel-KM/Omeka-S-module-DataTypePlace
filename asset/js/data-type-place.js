(function($) {

    $(document).on('o:prepare-value', function(e, dataType, value, valueObj) {
        if (dataType !== 'place') {
            return;
        }
        // console.log({dataType: dataType, value: value, valueObj: valueObj});

        const thisValue = $(value);
        const dataInput = thisValue.find('input[data-value-key="o:data"]');
        const uriInput = thisValue.find('input[data-value-key="@id"]');
        const valueDisplay = dataInput.closest('.input-body').find('.data-type-place');
        var valueValue = null;
        var oData;

        if (valueObj) {
            // "@id" and "o:data" are used in the real json-ld value.
            if (valueObj['o:data']) {
                // o:data may be json encoded or not.
                oData = valueObj['o:data'];
                if (typeof oData !== 'object') {
                    try {
                        valueValue = JSON.parse(oData);
                    } catch (ex) {
                    }
                } else {
                    valueValue = oData;
                }
            }
            // @deprecated.
            // The key "@value" was used with the previous hidden form.
            else if (valueObj['@value']) {
                try {
                    valueValue = JSON.parse(valueObj['@value']);
                } catch (ex) {
                }
            }
        }

        if (!valueValue || typeof valueValue !== 'object' || !Object.keys(valueValue).length) {
            valueValue = null;
        } else {
            valueValue['uri'] = valueObj['@id'] ?? valueValue['uri'] ?? null;
            if (!valueValue['uri']) {
                valueValue = null;
            }
        }

        if (valueValue) {
            dataInput.val(JSON.stringify(valueValue));
            uriInput.val(valueValue['uri']);
            $(valueDisplay).html(`<a href="${ valueValue['uri'] }">${ valueValue['toponym'] }</a>`
                + (valueValue['country'] ? ' (' + valueValue['country'] + ')' : '')
                + (valueValue['latitude'] && valueValue['longitude'] ? ' <span class="coordinates">[' + valueValue['latitude'] + '/' + valueValue['longitude'] + ']</span>': '')
            );
        } else {
            dataInput.val('');
            uriInput.val('');
            $(valueDisplay).empty();
        }

    });

})(jQuery);
