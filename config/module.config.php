<?php declare(strict_types=1);

namespace DataTypePlace;

return [
    'data_types' => [
        'invokables' => [
            'place' => DataType\Place::class,
        ],
        'value_annotating' => [
            'place',
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => dirname(__DIR__) . '/language',
                'pattern' => '%s.mo',
                'text_domain' => null,
            ],
        ],
    ],
    'csv_import' => [
        'data_types' => [
            'place' => [
                'label' => 'Place', // @translate
                // The data type "place" is an extended uri, where the value or the label is an array of data.
                // It may be simpler to use "literal" for some modules like CSV Import.
                /** @see \CSVImport\Mapping\PropertyMapping::processRow() */
                'adapter' => 'uri',
            ],
        ],
    ],
    'datatypeplace' => [
    ],
];
